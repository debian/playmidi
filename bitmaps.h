/************************************************************************
   bitmaps.h  --  bitmaps to be included in xplaymidi

   Copyright (C) 1996 Nathan I. Laredo

   This program is modifiable/redistributable under the terms
   of the GNU General Public Licence.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
   Send your comments and all your spare pocket change to
   laredo@gnu.ai.mit.edu (Nathan Laredo) or to PSC 1, BOX 709, 2401
   Kelly Drive, Lackland AFB, TX 78236-5128, USA.
 *************************************************************************/
#include "bitmaps/eject.bit"
#include "bitmaps/prev.bit"
#include "bitmaps/play.bit"
#include "bitmaps/next.bit"
#include "bitmaps/meter.bit"
